<?php

namespace ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wall
 */
class Wall
{

  /**
   * @var integer
   */
  private $id;

  /**
   * @var string
   */
  private $requestType;

  /**
   * @var string
   */
  private $requestValue;

  /**
   * @var \ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\User
   */
  private $user;


  /**
   * Get id
   *
   * @return integer
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set requestType
   *
   * @param string $requestType
   * @return Wall
   */
  public function setRequestType($requestType)
  {
      $this->requestType = $requestType;

      return $this;
  }

  /**
   * Get requestType
   *
   * @return string
   */
  public function getRequestType()
  {
      return $this->requestType;
  }

  /**
   * Set requestValue
   *
   * @param string $requestValue
   * @return Wall
   */
  public function setRequestValue($requestValue)
  {
      $this->requestValue = $requestValue;

      return $this;
  }

  /**
   * Get requestValue
   *
   * @return string
   */
  public function getRequestValue()
  {
      return $this->requestValue;
  }

  /**
   * Set user
   *
   * @param \ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\User $user
   * @return Wall
   */
  public function setUser(\ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\User $user = null)
  {
      $this->user = $user;

      return $this;
  }

  /**
   * Get user
   *
   * @return \ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\User
   */
  public function getUser()
  {
      return $this->user;
  }
}
