<?php

namespace ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WallType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('requestType')
            ->add('requestValue')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\Wall'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chalasdev_twitterfeedsbundle_wall';
    }
}
