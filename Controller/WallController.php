<?php

namespace ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Entity\Wall;
use ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Form\WallType;
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * Wall controller.
 *
 */
class WallController extends Controller
{

    /**
     * Lists all Wall entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->findAll();

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function mineAction()
    {
        $em = $this->getDoctrine()->getManager();
        $userId = $this->getUser();
        $entities = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->findByUser($userId);
        return $this->render('ChalasDevTwitterFeedsBundle:Wall:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Wall entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Wall();
        $entity->setUser($this->getUser());
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('wall_show', array('id' => $entity->getId())));
        }

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Wall entity.
     *
     * @param Wall $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Wall $entity)
    {
        $form = $this->createForm(new WallType(), $entity, array(
            'action' => $this->generateUrl('wall_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Wall entity.
     *
     */
    public function newAction()
    {
        $entity = new Wall();
        $form   = $this->createCreateForm($entity);

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Wall entity.
     *
     */
    public function showAction($id)
    {
        $content = '';
        $profil = '';
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wall entity.');
        }
        $request = $entity->getRequestValue();
        $consumId = $this->container->getParameter('client_id');
        $consumSecret = $this->container->getParameter('client_secret');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          $accessToken = $this->getUser()->getTwitterAccessToken();
          $accessSecret = $this->getUser()->getTwitterAccessTokenSecret();
          $connection = new TwitterOAuth($consumId, $consumSecret, $accessToken, $accessSecret);
          $profil = $connection->get('users/show', array('screen_name' => 'chalasdev'));
          $content = $connection->get('statuses/user_timeline', array('screen_name' => $request, 'count' => '25'));
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'content'     => $content,
            'profil'      => $profil
        ));
    }

    /**
     * Displays a form to edit an existing Wall entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wall entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Wall entity.
    *
    * @param Wall $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Wall $entity)
    {
        $form = $this->createForm(new WallType(), $entity, array(
            'action' => $this->generateUrl('wall_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Wall entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wall entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('wall_edit', array('id' => $id)));
        }

        return $this->render('ChalasDevTwitterFeedsBundle:Wall:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Wall entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ChalasDevTwitterFeedsBundle:Wall')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Wall entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('wall'));
    }

    /**
     * Creates a form to delete a Wall entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('wall_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
