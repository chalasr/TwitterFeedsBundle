<?php

namespace ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Abraham\TwitterOAuth\TwitterOAuth;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $content = '';
        $profil = '';
        $consumId = $this->container->getParameter('client_id');
        $consumSecret = $this->container->getParameter('client_secret');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
          $accessToken = $this->getUser()->getTwitterAccessToken();
          $accessSecret = $this->getUser()->getTwitterAccessTokenSecret();
          $connection = new TwitterOAuth($consumId, $consumSecret, $accessToken, $accessSecret);
          $content = $connection->get('statuses/home_timeline', array('count' => 25, 'exclude_replies' => true));
          $profil = $connection->get('users/show', array('screen_name' => 'chalasdev'));
        }
         return $this->render('ChalasDevTwitterFeedsBundle:Default:index.html.twig', array(
          'content' => $content,
          'profil'  => $profil
        ));
    }
}
