#
#TwitterFeedsBundle by Robin Chalas, built for Symfony2

Bundle built for integrate User management (FOSUserBundle),
Twitter OAuth authentication (HWIOAuthBundle),
and Walls management (GET favorite users timelines with abraham/twitteroauth)

##Requirements (before install)

- Symfony > 2.6
- HWIOAuthBundle(@dev) : ```composer require hwi/oauth-bundle @dev```

##Install:

Automatic :

```composer require chalasdev/twitterfeedsbundle @dev```

Manual:

Add the following line to your composer.json file :

```js
//composer.json
{
    //...

    "require": {
        //...
        "chalasdev/twitterfeedsbundle": "@dev"
    }

    //...
}
```

##Configure

You can find all TwitterFeedsBundle's configuration files in the ```Resources/config/app/``` folder of the bundle.

For an adapted app configuration, do :

```mv vendor/chalasdev/twitterfeedsbundle/ChalasDev/Bundle/ChalasDevTwitterFeedsBundle/Resources/config/app/* app/config/```

Now, update the parameters.yml file for your database infos and your Twitter API tokens.


Then, Add the following lines in your AppKernel.php file :

```php
//app/AppKernel.php
{
    //...

        new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),
        new ChalasDev\Bundle\ChalasDevTwitterFeedsBundle\ChalasDevTwitterFeedsBundle(),
        new FOS\UserBundle\FOSUserBundle(),

    //...
}
```

Install assets with this command :

```php app/console assets:install```

## Enjoy !
